package org.example.classes;

import static org.example.classes.BookFinder.*;

/**
 * Создать классы, спецификации которых приведены ниже. Определить конструкторы и методы setТип(),
 * getТип(), toString(). Определить дополнительно методы в классе, создающем массив объектов.
 * Задать критерий выбора данных и вывести эти данные на консоль. В каждом классе, обладающем информацией,
 * должно быть объявлено несколько конструкторов.
 * <p>
 * Book: id, Название, Автор (ы), Издательство, Год издания, Количество страниц, Цена, Тип переплета.
 * Создать массив объектов. Вывести:
 * a) список книг заданного автора;
 * b) список книг, выпущенных заданным издательством;
 * c) список книг, выпущенных после заданного года.
 */

public class MainTask {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book(1, "11.22.63", "Stephen King", "Scribner", 2011, 849, 15.40, "Hard");
        books[1] = new Book(2, "Fahrenheit 451", "Ray Bradbury", "Sterling Co.", 1967, 300, 10.99, "Soft");
        books[2] = new Book(3, "Animal Farm", "George Orwell", "Kalyani", 1975, 136, 2.89, "Soft");
        books[3] = new Book(4, "The Giver", "Lois Lowry", "HMH Books", 2014, 200, 5.99, "Hard");
        books[4] = new Book(5, "Catch-22", "Joseph Heller", "Sterling Co.", 2000, 250, 20.99, "Soft");

        findBooksByAuthor("Ray Bradbury", books);
        findBooksByPublisher("Sterling Co.", books);
        findBooksAfterYear(2000, books);
    }
}
