package org.example.classes;

public class BookFinder {
    public static void findBooksByAuthor(String author, Book[] books) {
        System.out.printf("Search result by author %s:\n", author);
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }

    public static void findBooksByPublisher(String publisher, Book[] books) {
        System.out.printf("Search result by publisher %s:\n", publisher);
        for (Book book : books) {
            if (book.getPublisher().equals(publisher)) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }

    public static void findBooksAfterYear(int year, Book[] books) {
        System.out.printf("Search result by  after year %d:\n", year);
        for (Book book : books) {
            if (book.getYear() > year) {
                System.out.println(book.toString());
            }
        }
        System.out.println();
    }
}
