package org.example.fundamentals;

import java.util.Scanner;

/**
 * Приветствовать любого пользователя при вводе его имени через командную строку
 */

public class MainTask1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name");
        String name = scanner.nextLine();
        System.out.printf("Hello %s!", name);
    }
}
