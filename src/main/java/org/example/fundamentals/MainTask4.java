package org.example.fundamentals;

import java.util.Arrays;

/**
 * Ввести целые числа как аргументы командной строки, подсчитать их сумму (произведение)
 * и вывести результат на консоль
 */

public class MainTask4 {
    public static void main(String[] args) {
        System.out.printf("Arguments: %s\n", Arrays.toString(args));
        int sum = 0;
        int product = 1;

        for (int i = 0; i < args.length; i++) {
            int number = Integer.parseInt(args[i]);
            sum += number;
            product *= number;
        }

        System.out.printf("Sum of numbers is %d\n", sum);
        System.out.printf("Product of numbers is %d", product);
    }
}
