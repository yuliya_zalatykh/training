package org.example.fundamentals;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Ввести число от 1 до 12. Вывести на консоль название месяца, соответствующего данному числу.
 * Осуществить проверку корректности ввода чисел
 */

enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER
}

public class MainTask5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of month");
        try {
            int number = scanner.nextInt();
            System.out.println(Month.values()[number - 1]);
        } catch (InputMismatchException | IndexOutOfBoundsException e) {
            System.out.println("Number isn't correct");
        }
    }
}
