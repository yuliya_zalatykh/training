package org.example.fundamentals;

import java.util.Random;
import java.util.Scanner;

/**
 * Вывести заданное количество случайных чисел с переходом и без перехода на новую строку
 */

public class MainTask3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Enter amount of random numbers");
        int amount = scanner.nextInt();
        int[] numbers = new int[amount];

        for (int i = 0; i < amount; i++) {
            numbers[i] = random.nextInt(10);
            System.out.println(numbers[i]);
        }
        System.out.println();

        for (int i = 0; i < amount; i++) {
            System.out.printf("%d ", numbers[i]);
        }
    }
}
