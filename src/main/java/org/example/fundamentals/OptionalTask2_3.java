package org.example.fundamentals;

import java.util.Random;
import java.util.Scanner;

/**
 * Ввести с консоли n - размерность матрицы a [n] [n]. Задать значения элементов матрицы в интервале
 * значений от -M до M с помощью генератора случайных чисел (класс Random).
 * Найти сумму элементов матрицы, расположенных между первым и вторым положительными элементами каждой строки
 */

public class OptionalTask2_3 {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter size of matrix");
        int size = scanner.nextInt();

        System.out.println("Enter random range");
        int range = scanner.nextInt();
        int[][] matrix = new int[size][size];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = random.nextInt(range * 2 + 1) - range;
                System.out.printf("%4d", matrix[i][j]);
            }
            System.out.println();
        }

        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length - 1; j++) {
                if (matrix[i][j] > 0) {
                    int rowSum = 0;
                    int k = j + 1;
                    while (k < matrix.length && matrix[i][k] <= 0) {
                        rowSum += matrix[i][k];
                        k++;
                    }
                    if (k == matrix.length) {
                        rowSum = 0;
                    }
                    sum += rowSum;
                    break;
                }
            }
        }
        System.out.printf("Sum of elements located between 1st and 2nd positive elements in each row is %d", sum);
    }
}
