package org.example.fundamentals;

import java.util.Random;
import java.util.Scanner;

/**
 * Задание. Ввести с консоли n - размерность матрицы a [n] [n].
 * Задать значения элементов матрицы в интервале значений от -M до M с помощью генератора случайных чисел (класс Random).
 * Упорядочить строки матрицы в порядке возрастания значений элементов k-го столбца.
 */

public class OptionalTask2_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Enter size of matrix");
        int size = scanner.nextInt();
        int[][] matrix = new int[size][size];

        System.out.println("Enter random range");
        int range = scanner.nextInt();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = random.nextInt(range * 2 + 1) - range;
                System.out.printf("%3d ", matrix[i][j]);
            }
            System.out.println();
        }

        System.out.printf("Choose the number of column (from 1 to %d) to sort ", size);
        int columnNumber = scanner.nextInt() - 1;

        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < matrix.length - 1; i++) {
                if (matrix[i][columnNumber] > matrix[i + 1][columnNumber]) {
                    for (int j = 0; j < matrix.length; j++) {
                        isSorted = false;
                        temp = matrix[i][j];
                        matrix[i][j] = matrix[i + 1][j];
                        matrix[i + 1][j] = temp;
                    }
                }
            }
        }

        System.out.println("Sorted matrix");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf("%3d ", matrix[i][j]);
            }
            System.out.println();
        }

    }
}