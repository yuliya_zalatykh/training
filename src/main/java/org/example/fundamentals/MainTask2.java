package org.example.fundamentals;

import java.util.Arrays;

/**
 * Отобразить в окне консоли аргументы командной строки в обратном порядке
 */

public class MainTask2 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        for (int i = args.length - 1; i >= 0; i--) {
            System.out.printf("%s ", args[i]);
        }
    }
}
